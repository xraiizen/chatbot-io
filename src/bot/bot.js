class Bot {
  constructor(name) {
    this.name = name;
    this.actions = {};
    this.history = [];
  }

  addAction(command, action) {
    this.actions[command] = action;
  }

  async performAction(command) {
    if (this.actions[command]) {
      const response = await this.actions[command]();
      this.history.push({ sender: this.name, message: response });
      return response;
    }
    return null;
  }

  help() {
    return Object.keys(this.actions);
  }
}

export default Bot;
